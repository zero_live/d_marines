# D Marines Game

I'm developing this game just for learn about create MUD games.

## System requirements

### with Docker

- `Docker version 18.06.1-ce or higher`
- `docker-compose version 1.22.0 or higher`

### with Ruby

- `ruby 2.5.1 or higher`

## Run tests

### with Docker

- `docker-compose up --build`
- `docker-compose run --rm server ruby tests/run.rb -v`

### with Ruby

- `ruby tests/run.rb -v`

## Running the server

Rigth now, the server always is running in port 4000.

### with Docker

- `docker-compose up --build`
- `docker-compose run --rm server ruby start.rb`

### with Ruby

First set environment variables:

- `ENIGMA_KEY=<word_16_characters_long>`
- `EN_TRANSLATIONS_FILE=<project_directory>/game/translations/collection/en.yml`

Then you can run the server with:

- `ruby start.rb`

## Work organization

The work that is being done in this project is organized on the next [board](https://trello.com/b/CiAOVROQ/d-marines).
