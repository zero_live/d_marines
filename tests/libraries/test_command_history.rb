require_relative '../../game/libraries/command_history/there_is_no_previous_command_error'
require_relative '../../game/libraries/command_history/there_is_no_current_command_error'
require_relative '../../game/libraries/command_history'
require_relative 'test_libraries'

class TestCommandHistory < TestLibraries
  def test_command_history_knows_current_command
    executed_command = 'any_command'
    history = CommandHistory.new
    history.register(executed_command)

    current_command = history.current_command

    assert_equal(current_command, executed_command)
  end

  def test_command_history_retrieves_previous_command
    executed_command = 'any_command'
    current_command = 'current_command'
    history = CommandHistory.new
    history.register(executed_command)
    history.register(current_command)

    previous_command = history.previous_command

    assert_equal(previous_command, executed_command)
  end

  def test_command_history_removes_previous_command_after_retrieve_it
    executed_command = 'any_command'
    current_command = 'current_command'
    expected_command = 'expected_command'
    history = CommandHistory.new
    history.register(expected_command)
    history.register(current_command)
    history.register(executed_command)
    history.register(current_command)

    history.previous_command

    previous_command = history.previous_command
    assert_equal(previous_command, expected_command)
  end

  def test_command_history_raises_an_error_if_there_is_no_previous_command
    executed_command = 'any_command'
    history = CommandHistory.new
    history.register(executed_command)

    assert_raise(CommandHistory::ThereIsNoPreviousCommandError) do
      history.previous_command
    end
  end

  def test_command_history_raises_an_error_if_there_is_no_current_command
    history = CommandHistory.new

    assert_raise(CommandHistory::ThereIsNoCurrentCommandError) do
      history.current_command
    end
  end

  def test_command_history_does_not_withdraw_current_command_if_there_is_no_previous_command
    executed_command = 'any_command'
    history = CommandHistory.new
    history.register(executed_command)

    begin
      history.previous_command
    rescue CommandHistory::ThereIsNoPreviousCommandError
    end

    current_command = history.current_command
    assert_equal(current_command, executed_command)
  end
end
