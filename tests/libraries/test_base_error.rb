require_relative '../../game/libraries/base_error'
require_relative 'test_libraries'

class TestBaseError < TestLibraries
  def test_base_error_has_to_have_a_message
    a_message = 'a_message'
    error = BaseError.new(a_message)

    error_message = error.message

    assert_equal(error_message, a_message)
  end

  def test_base_error_knows_to_build_its_self_with_the_same_anothers_message_error
    another_message_error = 'another_message_error'
    another_error = BaseError.new(another_message_error)
    error = BaseError.with(another_error)

    error_message = error.message

    assert_equal(error_message, another_message_error)
  end
end
