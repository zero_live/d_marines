require_relative '../../game/translations/service'
require_relative 'test_translations'

class TestService < TestTranslations
  def test_service_retrieves_a_text_for_a_label_and_locale
    label = 'd_marines'
    locale = 'en'

    translation = Translations::Service.for(label, locale)

    assert_equal(translation, 'D Marines')
  end

  def test_service_retrieves_missing_translation_for_a_missing_label
    label = 'missing_label'
    locale = 'en'

    translation = Translations::Service.for(label, locale)

    assert_equal(translation, 'Missing translation')
  end

  def test_service_retrieves_missing_locale_for_a_missing_locale
    label = 'd_marines'
    locale = 'non_existent_locale'

    translation = Translations::Service.for(label, locale)

    assert_equal(translation, 'Missing locale')
  end

  def test_service_has_a_default_locale
    label = 'd_marines'

    translation = Translations::Service.for(label)

    assert_equal(translation, 'D Marines')
  end
end
