require_relative '../../game/commands/welcome'
require_relative 'test_commands'
require_relative 'stub_client'

class TestWelcome < TestCommands
  def test_welcome_asks_login_or_signup
    client = StubClient.new

    execute_command { Commands::Welcome.execute_in(client) }

    assert(client.last_response?('Log in or Sign up'))
  end

  def test_welcome_can_execute_log_in
    client = StubClient.new
    client.add_action('login')

    execute_command { Commands::Welcome.execute_in(client) }

    assert(client.last_response?('Type your username'))
  end

  def test_welcome_can_execute_sign_up
    client = StubClient.new
    client.add_action('signup')

    execute_command { Commands::Welcome.execute_in(client) }

    assert(client.last_response?('Type your username'))
  end

  def test_welcome_go_to_its_self_if_user_does_not_choose_a_rigth_command
    client = StubClient.new
    client.add_action('incorrect_command')

    execute_command { Commands::Welcome.execute_in(client) }

    assert(client.last_response?('Log in or Sign up'))
  end
end
