require_relative '../../game/commands/select_marine'
require_relative '../../game/marines/service'
require_relative 'test_commands'
require_relative 'stub_client'

class TestSelectMarine < TestCommands
  def setup
    @client = StubClient.with_logged(username)
    create_marine
  end

  def test_select_marine_shows_available_marines

    execute_command { Commands::SelectMarine.execute_in(@client) }

    assert(@client.last_response?("0. #{user_marine}"))
  end

  def test_select_marine_can_select_a_marine_by_name
    @client.add_action(user_marine)

    execute_command { Commands::SelectMarine.execute_in(@client) }

    assert(@client.last_response?("Your marine #{user_marine} was selected"))
  end

  def test_select_marine_sends_to_marine_detail_after_select_a_marine
    @client.add_action(user_marine)

    execute_command { Commands::SelectMarine.execute_in(@client) }

    assert(@client.last_response?('You are in marinedetail'))
  end

  def test_select_marine_shows_an_error_when_tries_to_select_an_unexistent_marine
    unexisting_marine = 'no_marine'
    @client.add_action(unexisting_marine)

    execute_command { Commands::SelectMarine.execute_in(@client) }

    assert(@client.last_response?("Marine #{unexisting_marine} doesn't exist"))
  end

  private

  def create_marine
    Marines::Service.create(user_marine, username)
  end

  def username
    'username'
  end

  def user_marine
    'usermarine'
  end
end
