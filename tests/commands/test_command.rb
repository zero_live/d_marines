require_relative '../../game/commands/command/do_method_has_to_be_implemented_error'
require_relative '../../game/commands/command/needs_authorization_error'
require_relative '../../game/commands/command'
require_relative 'test_commands'
require_relative 'stub_client'

class TestCommand < TestCommands
  def test_command_shows_help_message_before_execute_command
    client = StubClient.new
    client.logged('username')

    begin
      execute_command { Commands::Command.execute_in(client) }
    rescue Commands::Command::DoMethodHasToBeImplementedError
    end

    assert(client.last_response?('Type help for know all the available commands'))
  end

  def test_commands_needs_a_logged_user_to_be_executed
    client = StubClient.new

    assert_raise(Commands::Command::NeedsAuthorizationError) do
      execute_command { CommandForTests.execute_in(client) }
    end
  end

  def test_commands_can_be_defined_as_no_autorized
    client = StubClient.new

    assert_nothing_raised do
      execute_command { UnautorizedCommandForTests.execute_in(client) }
    end
  end

  def test_commands_can_execute_help_in_any_question
    client = StubClient.new
    client.add_action('help')

    execute_command { UnautorizedCommandForTests.execute_in(client) }

    assert(client.last_response?('Type your command'))
  end

  def test_commands_can_execute_quit_in_any_question
    client = StubClient.new
    client.add_action('quit')

    execute_command { UnautorizedCommandForTests.execute_in(client) }

    assert(client.last_response?('Good Bye!'))
  end

  def test_commands_show_in_what_command_you_are
    client = StubClient.new

    execute_command { UnautorizedCommandForTests.execute_in(client) }

    assert(client.last_response?('You are in unautorizedcommandfortests'))
  end

  def test_you_can_use_back_command_in_any_place
    client = StubClient.new
    client.register(UnautorizedCommandForTests)
    client.add_action('back')

    execute_command { UnautorizedCommandForTests.execute_in(client) }

    assert(client.last_response?('Moving to previous command'))
  end

  private

  class CommandForTests < Commands::Command
    def do
    end
  end

  class UnautorizedCommandForTests < Commands::Command
    def do
      ask_to_client('Anything')
    end

    def unauthorized?
      true
    end
  end
end
