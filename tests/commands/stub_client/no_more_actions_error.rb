require_relative '../../../game/libraries/client'

class StubClient < Client
  class NoMoreActionsError < StandardError
  end
end
