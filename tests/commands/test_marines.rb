require_relative '../../game/commands/marines'
require_relative '../../game/marines/service'
require_relative 'test_commands'
require_relative 'stub_client'

class TestMarines < TestCommands
  def test_marines_can_be_created
    marine_name = 'any_marine'
    client = StubClient.new
    client.logged('username')
    client.add_action(marine_name)

    execute_command { Commands::Marines.execute_in(client) }

    assert(client.last_response?(marine_name))
  end

  def test_marines_cannot_be_created_with_short_name
    short_name = '1234'
    client = StubClient.new
    client.logged('username')
    client.add_action(short_name)

    execute_command { Commands::Marines.execute_in(client) }

    assert(client.last_response?("Marine's name must be at least 5 characters long"))
  end

  def test_marines_cannot_be_created_with_long_name
    short_name = '12345678901'
    client = StubClient.new
    client.logged('username')
    client.add_action(short_name)

    execute_command { Commands::Marines.execute_in(client) }

    assert(client.last_response?("Marine's name must be a maximum of 10 characters"))
  end

  def test_if_user_has_a_marine_saw_marines
    username = 'username'
    user_marine = 'usermarine'
    Marines::Service.create(user_marine, username)
    client = StubClient.new
    client.logged(username)

    execute_command { Commands::Marines.execute_in(client) }

    assert(client.last_response?(user_marine))
  end
end
