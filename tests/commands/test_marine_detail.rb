require_relative '../../game/commands/marine_detail'
require_relative '../../game/marines/service'
require_relative 'test_commands'
require_relative 'stub_client'

class TestMarineDetail < TestCommands
  def setup
    @client = StubClient.with_logged(username)
  end

  def test_marine_detail_shows_marine_stats
    Marines::Service.create(marine_name, username)
    @client.select_marine(marine_name)

    execute_command { Commands::MarineDetail.execute_in(@client) }

    assert(@client.last_response?("Name : #{marine_name}"))
  end

  def test_marine_detail_shows_an_error_if_marine_wasnt_selected_before

    execute_command { Commands::MarineDetail.execute_in(@client) }

    assert(@client.last_response?('You have to select a marine before do this action'))
  end

  def test_marine_detail_sends_to_select_marine_if_marine_wasnt_selected

    execute_command { Commands::MarineDetail.execute_in(@client) }

    assert(@client.last_response?('You are in selectmarine'))
  end

  def test_marine_detail_can_send_marines_to_choose_mission
    @client.select_marine(marine_name)

    execute_command { Commands::MarineDetail.execute_in(@client) }

    assert(@client.last_response?('What is the next order for the marine?'))
  end

  def test_marine_detail_sends_to_mission_after_choose_it
    Marines::Service.create(marine_name, username)
    @client.select_marine(marine_name)
    @client.add_action('missionlist')

    execute_command { Commands::MarineDetail.execute_in(@client) }

    assert(@client.last_response?('You are in missionlist'))
  end

  def test_marine_detail_show_an_error_for_unavailable_order
    Marines::Service.create(marine_name, username)
    @client.select_marine(marine_name)
    @client.add_action('unavailable_order')

    execute_command { Commands::MarineDetail.execute_in(@client) }

    assert(@client.last_response?('That order is not available'))
  end

  private

  def username
    'username'
  end

  def marine_name
    'marinename'
  end
end
