require_relative '../tests_collections/tests_collection_missions'
require_relative '../../game/commands/mission_list'
require_relative '../../game/marines/service'
require_relative 'stub_client'
require_relative 'test_commands'

class TestMarineList < TestCommands
  def test_marines_list_has_to_have_a_selected_marine
    client = StubClient.with_logged(username)

    execute_command { Commands::MissionList.execute_in(client) }

    assert(client.last_response?('You have to select a marine before do this action'))
  end

  def test_marine_list_shows_available_mission_message
    client = StubClient.with_logged(username)
    client.select_marine(marine_name)

    execute_command { Commands::MissionList.execute_in(client) }

    assert(client.last_response?("This are the mission availables for #{marine_name}"))
  end

  def test_marine_list_shows_available_mission_for_selected_marine
    available_missions = [a_mission, another_mission]
    store(available_missions)
    client = StubClient.with_logged(username)
    client.select_marine(marine_name)

    execute_command { Commands::MissionList.execute_in(client) }

    assert(client.last_response?("0. #{a_mission}"))
    assert(client.last_response?("1. #{another_mission}"))
  end

  def test_marine_list_show_how_order_a_mission
    client = StubClient.with_logged(username)
    client.select_marine(marine_name)

    execute_command { Commands::MissionList.execute_in(client) }

    assert(client.last_response?("Order a mission to a #{marine_name}, typing the missions number"))
  end

  private

  def store(missions)
    missions.each do |mission|
      TestsCollection::Missions.store(mission)
    end
  end

  def a_mission
    'A Mission'
  end

  def another_mission
    'Another Mission'
  end

  def username
    'username'
  end

  def marine_name
    'marinename'
  end
end
