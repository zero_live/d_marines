require_relative '../../game/users/collection'
require_relative '../../game/commands/login'
require_relative '../../game/users/user'
require_relative 'test_commands'
require_relative 'stub_client'

class TestLogIn < TestCommands

  def test_login_can_be_success
    username = 'zerolive'
    password = 'any_passw0rd'
    create_user(username, password)
    client = StubClient.new
    client.add_action(username)
    client.add_action(password)

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Login Success'))
  end

  def test_after_success_log_in_go_to_marines
    username = 'zerolive'
    password = 'any_passw0rd'
    create_user(username, password)
    client = StubClient.new
    client.add_action(username)
    client.add_action(password)

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Type the name of your first Marine'))
  end

  def test_cannot_log_in_with_a_user_that_does_not_exist
    client = StubClient.new
    client.add_action('login')
    client.add_action('non-user')
    client.add_action('password')

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Invalid Username or Password'))
  end

  def test_cannot_log_in_with_a_invalid_password
    username = 'zerolive'
    password = 'any_passw0rd'
    create_user(username, password)
    client = StubClient.new
    client.add_action(username)
    client.add_action('invalid_password')

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Invalid Username or Password'))
  end

  def test_cannot_do_log_in_with_a_sort_username
    short_username = '1234'
    client = StubClient.new
    client.add_action(short_username)
    client.add_action('password')

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Invalid Username or Password'))
  end

  def test_cannot_do_log_in_with_a_long_username
    long_username = '12345678901'
    client = StubClient.new
    client.add_action(long_username)
    client.add_action('password')

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Invalid Username or Password'))
  end

  def test_cannot_do_log_in_with_a_short_password
    short_password = '1234'
    client = StubClient.new
    client.add_action('username')
    client.add_action(short_password)

    execute_command { Commands::LogIn.execute_in(client) }

    assert(client.last_response?('Invalid Username or Password'))
  end
end
