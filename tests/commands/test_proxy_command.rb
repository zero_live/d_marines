require_relative '../../game/libraries/command_history/there_is_no_previous_command_error'
require_relative '../../game/libraries/command_history'
require_relative '../../game/commands/proxy_command'
require_relative 'test_commands'
require_relative 'stub_client'

class TestProxyCommand < TestCommands
  def test_proxy_commands_do_not_register_in_history
    client = StubClient.new
    client.register(TestProxy)
    client.add_action('back')

    execute_command { TestProxy.execute_in(client) }

    assert(client.last_response?('There is no previous command'))

  end

  def test_proxy_commands_do_not_show_help_information
    client = StubClient.new

    execute_command { TestProxy.execute_in(client) }

    assert(!client.last_response?('Type help for know all the available commands'))
  end

  def test_proxy_commands_do_not_show_where_the_user_is
    client = StubClient.new

    execute_command { TestProxy.execute_in(client) }

    assert(!client.last_response?("You are in #{TestProxy.word}"))
  end

  private

  class TestProxy < Commands::ProxyCommand
    def do
      ask_to_client('something')
    end

    private

    def unauthorized?
      true
    end
  end
end
