require_relative 'stub_client/no_more_actions_error'
require_relative '../test_game'

class TestCommands < TestGame

  private

  def execute_command(&block)
    block.call
  rescue StubClient::NoMoreActionsError
  end

  def create_user(username, password)
    user = Users::User.new(username, password, password)

    Users::Collection.insert(user)
  end
end
