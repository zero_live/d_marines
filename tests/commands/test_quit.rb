require_relative '../../game/commands/quit'
require_relative 'test_commands'
require_relative 'stub_client'

class TestQuit < TestCommands
  def test_help_shows_welcome_command
    client = StubClient.new

    execute_command { Commands::Quit.execute_in(client) }

    assert(client.last_response?('Good Bye!'))
  end
end
