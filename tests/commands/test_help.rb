require_relative '../../game/commands/help'
require_relative 'stub_client'
require_relative 'test_commands'

class TestHelp < TestCommands
  def test_help_shows_welcome_command
    client = StubClient.new

    execute_command { Commands::Help.execute_in(client) }

    assert(client.last_response?('welcome'))
  end

  def test_help_shows_log_in_command
    client = StubClient.new

    execute_command { Commands::Help.execute_in(client) }

    assert(client.last_response?('login'))
  end

  def test_help_does_not_show_marines_command_without_logged_user
    client = StubClient.new

    execute_command { Commands::Help.execute_in(client) }

    assert(!client.last_response?('marines'))
  end

  def test_help_shows_marines_command_with_logged_user
    client = StubClient.new
    client.logged('username')

    execute_command { Commands::Help.execute_in(client) }

    assert(client.last_response?('marines'))
  end

  def test_help_can_execute_any_available_command
    available_command = 'welcome'
    client = StubClient.new
    client.add_action(available_command)

    execute_command { Commands::Help.execute_in(client) }

    assert(client.last_response?('Log in or Sign up'))
  end

  def test_help_warns_if_asked_command_is_not_available
    available_command = 'not_available_command'
    client = StubClient.new
    client.add_action(available_command)

    execute_command { Commands::Help.execute_in(client) }

    assert(client.last_response?("Typped command isn't available"))
  end
end
