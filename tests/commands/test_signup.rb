require_relative '../../game/commands/signup'
require_relative 'test_commands'
require_relative 'stub_client'

class TestSignUp < TestCommands
  def test_after_successful_sign_up_user_goes_to_log_in
    username = 'username'
    password = 'password'
    client = StubClient.new
    client.add_action(username)
    client.add_action(password)
    client.add_action(password)
    client.add_action(username)
    client.add_action(password)
    client.add_action(password)

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('Login Success'))
  end

  def test_cannot_sign_up_the_same_username_twice
    username = 'a_username'
    password = 'password'
    client = StubClient.new
    create_user(username, password)
    client.add_action(username)
    client.add_action('password')
    client.add_action('password')

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('That Username is already taken'))
  end

  def test_cannot_sign_up_without_repeat_the_same_password
    client = StubClient.new
    client.add_action('a_username')
    client.add_action('a_password')
    client.add_action('another_password')

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('You have not repeated the same password'))
  end

  def test_sign_up_does_not_allow_short_usernames
    short_username = '1234'
    password = 'a_password'
    client = StubClient.new
    client.add_action(short_username)
    client.add_action(password)
    client.add_action(password)

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('Username must be at least 5 characters long'))
  end

  def test_sign_up_does_not_allow_long_usernames
    long_username = '0123456789a'
    password = 'a_password'
    client = StubClient.new
    client.add_action(long_username)
    client.add_action(password)
    client.add_action(password)

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('Username must be a maximum of 10 characters'))
  end

  def test_sign_up_when_repeats_the_same_password
    password = 'a_password'
    client = StubClient.new
    client.add_action('a_username')
    client.add_action(password)
    client.add_action(password)

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('Sign up success'))
  end

  def test_sign_up_does_not_allow_short_pawsswords
    short_password = '1234'
    client = StubClient.new
    client.add_action('username')
    client.add_action(short_password)
    client.add_action(short_password)

    execute_command { Commands::SignUp.execute_in(client) }

    assert(client.last_response?('Password must be at least 8 characters long'))
  end
end
