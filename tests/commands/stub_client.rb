require_relative '../../game/libraries/command_history'
require_relative 'stub_client/no_more_actions_error'
require_relative '../../game/libraries/client'

class StubClient < Client
  def self.with_logged(user)
    client = self.new
    client.logged(user)
    client
  end

  def initialize
    super(:client)
    @responses = []
    @actions = []
  end

  def add_action(action)
    @actions.push(action)
  end

  def wait_client_for(text)
    @responses.push(text)

    send_next_action
  end

  def send_to_client(text)
    @responses.push(text)
  end

  def responses
    @responses
  end

  def last_response?(text)
    regex = Regexp.new("\w*(#{text})\w*")

    @responses.any? do |response|
      response.match(regex)
    end
  end

  def close
  end

  private

  def send_next_action
    action = @actions.shift
    raise NoMoreActionsError if action.nil?
    action
  end
end
