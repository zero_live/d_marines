require_relative '../../game/commands/create_marine'
require_relative '../../game/marines/service'
require_relative 'test_commands'
require_relative 'stub_client'

class TestCreateMarine < TestCommands
  def test_user_cannot_have_more_than_one_marine
    username = 'username'
    user_marine = 'usermarine'
    Marines::Service.create(user_marine, username)
    client = StubClient.new
    client.logged(username)
    client.add_action('newmarine')

    execute_command { Commands::CreateMarine.execute_in(client) }

    assert(client.last_response?('You still can not have a second marine'))
  end

  def test_user_can_create_a_marine
    client = StubClient.new
    client.logged('username')
    client.add_action('usermarine')

    execute_command { Commands::CreateMarine.execute_in(client) }

    assert(client.last_response?('Your marine was created'))
  end

  def test_user_cannot_create_a_marine_with_short_name
    client = StubClient.new
    client.logged('username')
    client.add_action('1234')

    execute_command { Commands::CreateMarine.execute_in(client) }

    assert(client.last_response?("Marine's name must be at least 5 characters long"))
  end

  def test_user_cannot_create_a_marine_with_long_name
    client = StubClient.new
    client.logged('username')
    client.add_action('12345678901')

    execute_command { Commands::CreateMarine.execute_in(client) }

    assert(client.last_response?("Marine's name must be a maximum of 10 characters"))
  end
end
