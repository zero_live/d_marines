require_relative '../../game/missions/collection'
require_relative '../../game/missions/mission'

module TestsCollection
  class Missions < Missions::Collection
    class << self
      def flush
        @@collection = []
      end

      def store(mission)
        builded = ::Missions::Mission.new(mission)

        @@collection.push(builded.to_hash)
      end
    end
  end
end
