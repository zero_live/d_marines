require_relative '../../game/marines/collection'

module TestsCollection
  class Marines < Marines::Collection
    def self.flush
      @@collection = []
    end
  end
end
