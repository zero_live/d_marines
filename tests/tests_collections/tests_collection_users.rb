require_relative '../../game/users/collection'

module TestsCollection
  class Users < Users::Collection
    def self.flush
      @@collection = []
    end
  end
end
