require_relative 'commands/test_create_marine'
require_relative 'commands/test_select_marine'
require_relative 'commands/test_marine_detail'
require_relative 'commands/test_proxy_command'
require_relative 'commands/test_mission_list'
require_relative 'commands/test_command'
require_relative 'commands/test_welcome'
require_relative 'commands/test_marines'
require_relative 'commands/test_signup'
require_relative 'commands/test_login'
require_relative 'commands/test_help'
require_relative 'commands/test_quit'

require_relative 'libraries/test_command_history'
require_relative 'libraries/test_base_error'

require_relative 'translations/test_service'
