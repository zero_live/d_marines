ENV['ENVIRONMENT'] = 'test'

require_relative 'tests_collections/tests_collection_missions'
require_relative 'tests_collections/tests_collection_marines'
require_relative 'tests_collections/tests_collection_users'
require 'test/unit'

class TestGame < Test::Unit::TestCase
  def setup
    TestsCollection::Missions.flush
    TestsCollection::Marines.flush
    TestsCollection::Users.flush
  end

  def teardown
    TestsCollection::Missions.flush
    TestsCollection::Marines.flush
    TestsCollection::Users.flush
  end
end
