require_relative 'command/needs_authorization_error'
require_relative 'command'

module Commands
  class ProxyCommand < Command
    def self.execute_in(client)
      client.send_to_client("\n")

      self.new(client).do
    end

    def initialize(client)
      @client = client

      require_authorization!
    end
  end
end
