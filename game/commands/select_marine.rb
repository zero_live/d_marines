require_relative '../marines/service'
require_relative 'marine_detail'
require_relative 'command'

module Commands
  class SelectMarine < Command
    def self.description
      t('select_marine_description')
    end

    def do
      loop do
        send_to_client(t('available_marines'))
        print_marine_list
        marine = ask_to_client(t('select_marine'))

        if exists?(marine)
          send_to_client(t('marine_selected') % marine)
          store_selected(marine)
          execute(Commands::MarineDetail)
        end

        send_to_client(t('marine_not_exist') % marine)
      end
    end

    private

    def print_marine_list
      marines = ::Marines::Service.collect_for(current_user)

      marines.each_with_index do |marine, index|
        send_to_client("#{index}. #{marine['name']}")
      end
    end

    def exists?(marine)
      ::Marines::Service.exists?(marine, current_user)
    end

    def store_selected(marine)
      @client.select_marine(marine)
    end
  end
end
