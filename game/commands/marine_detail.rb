require_relative '../../game/commands/select_marine'
require_relative 'mission_list'
require_relative 'command'

module Commands
  class MarineDetail < Command
    def self.description
      t('marine_detail_description')
    end

    def do
      loop do
        unless is_marine_selected?
          send_to_client(t('select_marine_before'))
          execute(Commands::SelectMarine)
        end

        send_to_client("Name : #{marine_name}")
        send_to_client(t('available_orders'))
        order = ask_to_client(t('marine_next_order'))

        execute(Commands::MissionList) if is_mission_list?(order)

        send_to_client(t('unavailable_order'))
      end
    end

    private

    def is_mission_list?(order)
      order == Commands::MissionList.word
    end

    def marine_name
      @client.selected_marine
    end

    def is_marine_selected?
      @client.is_marine_selected?
    end
  end
end
