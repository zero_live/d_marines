require_relative '../users/service'
require_relative 'command'
require_relative 'login'

module Commands
  class SignUp < Command
    USER_ERRORS = [
      Users::Service::UsernameAlreadyTakenError,
      Users::Service::UsernameTooShortError,
      Users::Service::UsernameTooLongError,
      Users::Service::DifferentPasswordsError,
      Users::Service::PasswordTooShortError
    ]

    def self.description
      t('signup_description')
    end

    def do
      username = ask_to_client(t('type_username'))
      password = ask_to_client(t('type_password'))
      repeated_password = ask_to_client(t('repeat_password'))

      begin
        Users::Service.create(username, password, repeated_password)
        send_to_client(t('signup_success'))
        go_to_log_in
      rescue *USER_ERRORS => error
        send_to_client(error.message)
        go_to_sign_in
      end
    end

    private

    def go_to_log_in
      execute(Commands::LogIn)
    end

    def go_to_sign_in
      self.do
    end

    def unauthorized?
      true
    end
  end
end
