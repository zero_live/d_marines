require_relative 'command'
require_relative 'signup'
require_relative 'login'

module Commands
  class Welcome < Command
    def self.description
      t('welcome_description')
    end

    def do
      command = ask_to_client(t('do_login_signup'))

      execute(Commands::LogIn) if Commands::LogIn.is?(command)
      execute(Commands::SignUp) if Commands::SignUp.is?(command)

      go_to_welcome
    end

    private

    def go_to_welcome
      self.do
    end

    def unauthorized?
      true
    end
  end
end
