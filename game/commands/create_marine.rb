require_relative '../marines/service'
require_relative 'select_marine'
require_relative 'command'

module Commands
  class CreateMarine < Command
    MARINE_ERRORS = [
      ::Marines::Service::MarineNameTooShortError,
      ::Marines::Service::MarineNameTooLongError
    ]

    def self.description
      t('create_marine_description')
    end

    def do
      if has_current_user_marines?
        send_to_client(t('second_marine_not_allowed'))
      else
        marine_name = ask_to_client(t('marine_name'))
        marine = ::Marines::Service.create(marine_name, current_user)
        send_to_client(t('marine_created'))
      end

      go_to_select_marine
    rescue *MARINE_ERRORS => error
      send_to_client(error.message)
      go_to_create_marine
    end

    private

    def has_current_user_marines?
      ::Marines::Service.has_any_marines?(current_user)
    end

    def go_to_select_marine
      execute(Commands::SelectMarine)
    end

    def go_to_create_marine
      self.do
    end
  end
end
