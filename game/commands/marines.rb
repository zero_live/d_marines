require_relative '../marines/service'
require_relative 'proxy_command'
require_relative 'create_marine'
require_relative 'select_marine'

module Commands
  class Marines < ProxyCommand
    def self.description
      t('marines_description')
    end

    def do
      execute(Commands::CreateMarine) unless has_current_users_marines?

      execute(Commands::SelectMarine)
    end

    private

    def has_current_users_marines?
      ::Marines::Service.has_any_marines?(current_user)
    end
  end
end
