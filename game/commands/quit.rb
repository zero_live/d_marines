require_relative 'proxy_command'

module Commands
  class Quit < ProxyCommand
    def self.description
      t('quit_description')
    end

    def do
      send_to_client(t('good_bye'))

      close_connection
    end

    private

    def close_connection
      @client.close
    end

    def unauthorized?
      true
    end
  end
end
