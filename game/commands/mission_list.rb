require_relative '../missions/service'
require_relative 'command'

module Commands
  class MissionList < Command
    def self.description
      t('mission_list_description')
    end

    def do
      unless is_marine_selected?
        send_to_client(t('select_marine_before'))
        go_to_select_marine
      end

      send_to_client(t('available_mission') % marine_name)
      print_mission_list

      mission = ask_to_client(t('order_mission_marine') % marine_name)
      go_to_mission_list
    end

    private

    def print_mission_list
      missions = find_missions
      mission_number = 0

      return if missions.empty?

      missions.each do |mission|
        send_to_client("#{mission_number}. #{mission['title']}")
        mission_number += 1
      end
    end

    def go_to_select_marine
      execute(Commands::SelectMarine)
    end

    def marine_name
      @client.selected_marine
    end

    def is_marine_selected?
      @client.is_marine_selected?
    end

    def find_missions
      Missions::Service.retrieve
    end

    def go_to_mission_list
      self.do
    end
  end
end
