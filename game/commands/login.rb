require_relative 'welcome'
require_relative 'command'
require_relative 'marines'

module Commands
  class LogIn < Command
    def self.description
      t('login_description')
    end

    def do
      username = ask_to_client(t('type_username'))
      password = ask_to_client(t('type_password'))

      unless Users::Service.exists?(username, password)
        send_to_client(t('invalid_name_password'))
        go_to_log_in
      end

      @client.logged(username)
      send_to_client(t('login_success'))
      go_to_marines
    end

    private

    def go_to_log_in
      self.do
    end

    def go_to_marines
      execute(Commands::Marines)
    end

    def go_to_welcome
      execute(Commands::Welcome)
    end

    def is_welcome_command?(answer)
      Commands::Welcome.is?(answer)
    end

    def unauthorized?
      true
    end
  end
end
