require_relative 'command/do_method_has_to_be_implemented_error'
require_relative 'command/authorizable'
require_relative 'command/translatable'
require_relative 'command/comunicable'
require_relative 'command/executable'

module Commands
  class Command
    include Authorizable
    include Translatable
    include Comunicable
    include Executable

    class << self
      def is?(command)
        word == command
      end

      def word
        self.name.split('::').last.downcase
      end

      def description
        t('command_description')
      end
    end

    def initialize(client)
      @client = client
      @client.register(self.class)

      require_authorization!
    end

    def do
      raise DoMethodHasToBeImplementedError
    end
  end
end
