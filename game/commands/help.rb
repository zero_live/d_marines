require_relative 'command/available_commands'
require_relative 'proxy_command'

module Commands
  class Help < ProxyCommand
    def self.description
      t('help_description')
    end

    def do
      print_available_commands

      loop do
        command = ask_to_client(t('type_command'))
        run(command)
        send_to_client(t('unavailable_command'))
      end
    end

    private

    def print_available_commands
      available_commands.each do |command|
        name = command.word
        description = command.description

        send_to_client("#{name} - #{description}")
      end
    end

    def run(commando)
      available_commands.each do |command|
        execute(command) if command.is?(commando)
      end
    end

    def available_commands
      AvailableCommands.list_for(@client)
    end

    def unauthorized?
      true
    end
  end
end
