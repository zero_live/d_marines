require_relative '../../libraries/base_error'

module Commands
  class Command
    class NeedsAuthorizationError < BaseError
    end
  end
end
