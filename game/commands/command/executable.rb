require_relative '../back'
require_relative '../help'
require_relative '../quit'

module Commands
  class Command
    module Executable
      COMMON_COMMANDS = [::Commands::Back, ::Commands::Help, ::Commands::Quit]

      private

      def self.included(base)
        base.extend ClassMethods
      end

      def execute_common_command_if_is_in(answer)
        COMMON_COMMANDS.each do |command|
          execute(command) if command.is?(answer)
        end
      end

      def execute(command)
        command.execute_in(@client)
      end

      module ClassMethods
        def execute_in(client)
          client.send_to_client(t('help_info'))

          command_name = self.word
          client.send_to_client(t('you_are_in') % command_name)

          self.new(client).do
        end
      end
    end
  end
end
