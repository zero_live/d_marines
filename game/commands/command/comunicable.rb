
module Commands
  class Command
    module Comunicable

      private

      def ask_to_client(text)
        answer = @client.wait_client_for(text)

        execute_common_command_if_is_in(answer)

        answer
      end

      def send_to_client(text)
        @client.send_to_client(text)

        nil
      end
    end
  end
end
