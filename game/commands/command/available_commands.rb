require_relative 'needs_authorization_error'
require_relative '../command'

module Commands
  class Command
    class AvailableCommands
      class << self
        def list_for(client)
          available = []
          commands = ObjectSpace.each_object(Class).select { |klass| klass < Command }

          available = commands.select do |command|
            available?(command, client)
          end

          available
        end

        private

        def available?(command, client)
          result = true

          begin
            command.new(client)
          rescue Commands::Command::NeedsAuthorizationError
            result = false
          end

          result
        end
      end
    end
  end
end
