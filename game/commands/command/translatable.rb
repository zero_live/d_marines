require_relative '../../translations/service'

module Commands
  class Command
    DEFAULT_LOCALE = 'en'

    module Translatable

      private

      def self.included(base)
        base.extend ClassMethods
      end

      def t(label, locale=DEFAULT_LOCALE)
        text = Translations::Service.for(label, locale)

        text
      end

      module ClassMethods
        def t(label, locale=DEFAULT_LOCALE)
          text = Translations::Service.for(label, locale)

          text
        end
      end
    end
  end
end
