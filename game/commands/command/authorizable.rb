require_relative 'needs_authorization_error'

module Commands
  class Command
    module Authorizable

      private

      def unauthorized?
        false
      end

      def require_authorization!
        raise Command::NeedsAuthorizationError.new(t('auth_error')) if autorized_command_has_not_logged_user?
      end

      def autorized_command_has_not_logged_user?
        !(logged_user? || unauthorized?)
      end

      def current_user
        @client.current_user
      end

      def logged_user?
        @client.logged_user?
      end
    end
  end
end
