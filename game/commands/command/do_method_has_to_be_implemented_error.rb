
module Commands
  class Command
    class DoMethodHasToBeImplementedError < StandardError
    end
  end
end
