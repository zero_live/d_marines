require_relative '../libraries/client/there_is_no_previous_command_error'
require_relative 'proxy_command'
require_relative 'welcome'

module Commands
  class Back < ProxyCommand
    def self.description
      t('proxy_description')
    end

    def do
      move_to_previous_command
    rescue Client::ThereIsNoPreviousCommandError
      send_to_client(t('no_previous_command'))

      move_to_current_command
    end

    private

    def move_to_previous_command
      previous_command = @client.previous_command
      send_to_client(t('moving_previous'))

      execute(previous_command)
    end

    def move_to_current_command
      current_command = @client.current_command

      execute(current_command)
    end

    def unauthorized?
      true
    end
  end
end
