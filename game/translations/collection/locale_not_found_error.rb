require_relative '../../libraries/base_error'

module Translations
  class Collection
    class LocaleNotFoundError < BaseError
    end
  end
end
