require_relative '../../libraries/base_error'

module Translations
  class Collection
    class LabelNotFoundError < BaseError
    end
  end
end
