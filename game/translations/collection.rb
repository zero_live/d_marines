require_relative 'collection/locale_not_found_error'
require_relative 'collection/label_not_found_error'
require 'yaml'

module Translations
  class Collection

    class << self
      def find(label, at:)
        raise LocaleNotFoundError.new(missing_locale_message) unless exists?(at)

        text = collection[at][label]
        raise LabelNotFoundError.new(missing_translation_message) if text.nil?

        text
      end

      private

      def exists?(locale)
        !collection[locale].nil?
      end

      def collection
        @collection ||= YAML.load_file(ENV['EN_TRANSLATIONS_FILE'])
      end

      def missing_locale_message
        collection['missing_locale']
      end

      def missing_translation_message
        collection['missing_translation']
      end
    end
  end
end
