require_relative 'collection/locale_not_found_error'
require_relative 'collection/label_not_found_error'
require_relative 'collection'

module Translations
  class Service
    class << self
      DEFAULT_LOCALE = 'en'
      COLLECTION_ERRORS = [
        Collection::LocaleNotFoundError,
        Collection::LabelNotFoundError
      ]

      def for(label, locale=DEFAULT_LOCALE)
        text = Collection.find(label, at:locale)

        text
      rescue *COLLECTION_ERRORS => error
        return error.message
      end
    end
  end
end
