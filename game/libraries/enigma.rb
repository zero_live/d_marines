require "openssl"

class Enigma
  class << self
    ALGORITHM = "AES-128-ECB"

    def encrypt(phrase)
      encrypter = OpenSSL::Cipher.new(ALGORITHM).encrypt
      encrypter.key = ENV['ENIGMA_KEY']

      encrypter.update(phrase) + encrypter.final
    end

    def decrypt(phrase)
      decrypter = OpenSSL::Cipher.new(ALGORITHM).decrypt
      decrypter.key = ENV['ENIGMA_KEY']

      decrypter.update(phrase) + decrypter.final
    end
  end
end
