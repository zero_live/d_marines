require_relative 'command_history/there_is_no_previous_command_error'
require_relative 'client/there_is_no_previous_command_error'
require_relative 'command_history'
require_relative 'logeable'

class Client
  include Logeable

  def initialize(client)
    @client = client
    @logged_user = nil
    @history = CommandHistory.new
    @marine = nil

    log_info("Connection accepted")
  end

  def send_to_client(message)
    @client.puts(message)
  end

  def wait_client_for(message)
    send_to_client(message)

    command = nil
    while command.nil?
      command = @client.gets
      sleep 0.1
    end

    command.strip
  end

  def close
    @client.close
    log_info("Connection clossed")
  end

  def logged(user)
    @logged_user = user
  end

  def current_user
    @logged_user
  end

  def logged_user?
    !@logged_user.nil?
  end

  def register(command)
    @history.register(command)
  end

  def current_command
    @history.current_command
  end

  def previous_command
    command = @history.previous_command

    command
  rescue CommandHistory::ThereIsNoPreviousCommandError
    raise ThereIsNoPreviousCommandError.new
  end

  def select_marine(marine)
    @marine = marine
  end

  def selected_marine
    @marine
  end

  def is_marine_selected?
    !@marine.nil?
  end
end
