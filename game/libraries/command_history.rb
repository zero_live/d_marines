require_relative 'command_history/there_is_no_previous_command_error'
require_relative 'command_history/there_is_no_current_command_error'

class CommandHistory
  def initialize
    @history = []
  end

  def register(command)
    @history.push(command)
  end

  def current_command
    raise ThereIsNoCurrentCommandError.new unless exists_current_command?
    command = @history.last

    command
  end

  def previous_command
    raise ThereIsNoPreviousCommandError.new unless exists_previous_command?
    command = withdraw_until_previous_command

    command
  end

  private

  def withdraw_until_previous_command
    remove_current_command_from_history
    command = withdraw_previous_command

    command
  end

  def remove_current_command_from_history
    @history.pop
  end

  def withdraw_previous_command
    command = @history.pop

    command
  end

  def exists_current_command?
    command = @history.last

    !command.nil?
  end

  def exists_previous_command?
    two_commands_back = -2

    command = @history[two_commands_back]

    !command.nil?
  end
end
