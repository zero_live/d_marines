require 'logger'

module Logeable
  PROC_OUTPUT = '/proc/1/fd/1'

  def log_info(message)
    logger.info(message) unless ENV['ENVIRONMENT'] == 'test'
  end

  private

  def logger
    @logger ||= Logger.new(PROC_OUTPUT)
  end
end
