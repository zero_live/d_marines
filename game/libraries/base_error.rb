
class BaseError < StandardError
  def self.with(error)
    message = error.message

    self.new(message)
  end

  def initialize(message)
    @message = message
  end

  def message
    @message
  end
end
