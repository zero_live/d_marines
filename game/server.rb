require_relative 'commands/command/needs_authorization_error'
require_relative 'libraries/logeable'
require_relative 'libraries/client'
require_relative 'commands/welcome'
require_relative 'commands/quit'
require 'socket'

class Server
  include Logeable

  def initialize(port:4000)
    @tcp = TCPServer.new(port)
  end

  def start
    log_info('Server started')
    thread_for { go_to_welcome }
  end

  private

  def thread_for(&block)
    loop do
      thread = Thread.start(@tcp.accept) do |client|
        @client = Client.new(client)

        begin
          block.call
        rescue Commands::Command::NeedsAuthorizationError => error
          @client.send_to_client(error.message)
          go_to_welcome
        rescue IOError
          thread.terminate
        end

        close_connection
      end
    end
    log_info('Server stopped')
  end

  def go_to_welcome
    Commands::Welcome.execute_in(@client)
  end

  def close_connection
    Commands::Quit.execute_in(@client)
  end
end
