
module Missions
  class Mission
    def self.from(item)
      title = item['title']

      self.new(title)
    end

    def initialize(title)
      @title = title
    end

    def to_hash
      {
        'title' => @title
      }
    end
  end
end
