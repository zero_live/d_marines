require_relative 'collection'

module Missions
  class Service
    def self.retrieve
      list = Collection.all

      missions = list.map do |mission|
        mission.to_hash
      end

      missions
    end
  end
end
