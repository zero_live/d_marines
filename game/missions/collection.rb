require_relative 'mission'

module Missions
  class Collection
    @@collection = []

    def self.all
      missions = @@collection.map do |item|
        Mission.from(item)
      end

      missions
    end
  end
end
