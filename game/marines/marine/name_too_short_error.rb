require_relative '../../libraries/base_error'

module Marines
  class Marine
    class NameTooShortError < BaseError
    end
  end
end
