require_relative '../../libraries/base_error'

module Marines
  class Marine
    class NameTooLongError < BaseError
    end
  end
end
