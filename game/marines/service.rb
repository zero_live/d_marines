require_relative 'service/marine_name_too_short_error'
require_relative 'service/marine_name_too_long_error'
require_relative 'collection/marine_not_found_error'
require_relative 'marine/name_too_short_error'
require_relative 'marine/name_too_long_error'
require_relative 'collection'
require_relative 'marine'

module Marines
  class Service
    class << self
      MARINE_ERRORS = {
        Marine::NameTooShortError => MarineNameTooShortError,
        Marine::NameTooLongError => MarineNameTooLongError
      }

      def create(name, owner)
        begin
          marine = Marine.new(name, owner)

          created_marine = Collection.insert(marine)

          created_marine.to_hash
        rescue => rescued_error
          raise error_for(rescued_error)
        end
      end

      def has_any_marines?(user)
        marines = Collection.find_by_owner(user)

        !marines.empty?
      end

      def collect_for(user)
        marines = Collection.find_by_owner(user)

        marines
      end

      def exists?(marine, user)
        result = true

        begin
          Collection.find(marine, user)
        rescue Collection::MarineNotFoundError
          result = false
        end

        result
      end

      private

      def error_for(rescued_error)
        error = MARINE_ERRORS[rescued_error.class]

        return rescued_error if error.nil?

        error.with(rescued_error)
      end
    end
  end
end
