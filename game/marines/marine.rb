require_relative 'marine/name_too_short_error'
require_relative 'marine/name_too_long_error'
require_relative '../translations/service'

module Marines
  class Marine
    NAME_MINIMUM_LENGHT = 5
    NAME_MAXIMUM_LENGHT = 10

    def self.from(item)
      name = item['name']
      owner = item['owner']

      self.new(name, owner)
    end

    def initialize(name, owner)
      @name = name
      @owner = owner

      raise NameTooShortError.new(marine_name_short_message) unless is_name_enougth_long?
      raise NameTooLongError.new(marine_name_long_message) if is_name_long?
    end

    def belongs_to?(username)
      @owner == username
    end

    def is?(name)
      @name == name
    end

    def to_hash
      {
        'name' => @name
      }
    end

    def to_store
      {
        'name' => @name,
        'owner' => @owner
      }
    end

    private

    def is_name_enougth_long?
      @name.size >= NAME_MINIMUM_LENGHT
    end

    def is_name_long?
      @name.size > NAME_MAXIMUM_LENGHT
    end

    def marine_name_short_message
      Translations::Service.for('marine_name_short_message') % NAME_MINIMUM_LENGHT
    end

    def marine_name_long_message
      Translations::Service.for('marine_name_long_message') % NAME_MAXIMUM_LENGHT
    end
  end
end
