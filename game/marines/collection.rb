require_relative 'collection/marine_not_found_error'

module Marines
  class Collection
    class << self
      @@collection = []

      def insert(marine)
        @@collection.push(marine.to_store)

        marine
      end

      def find_by_owner(owner)
        @@collection.select do |item|
          marine = Marine.from(item)

          marine.belongs_to?(owner)
        end
      end

      def find(name, owner)
        raw_marine = @@collection.select do |item|
          marine = Marine.from(item)

          marine.belongs_to?(owner) && marine.is?(name)
        end

        raise MarineNotFoundError.new if raw_marine.empty?

        Marine.from(raw_marine.first)
      end
    end
  end
end
