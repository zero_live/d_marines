require_relative '../../libraries/base_error'

module Marines
  class Service
    class MarineNameTooLongError < BaseError
    end
  end
end
