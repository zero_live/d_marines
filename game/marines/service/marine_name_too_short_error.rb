require_relative '../../libraries/base_error'

module Marines
  class Service
    class MarineNameTooShortError < BaseError
    end
  end
end
