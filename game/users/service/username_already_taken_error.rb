require_relative '../../libraries/base_error'

module Users
  class Service
    class UsernameAlreadyTakenError < BaseError
    end
  end
end
