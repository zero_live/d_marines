require_relative '../../libraries/base_error'

module Users
  class Service
    class PasswordTooShortError < BaseError
    end
  end
end
