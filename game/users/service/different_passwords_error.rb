require_relative '../../libraries/base_error'

module Users
  class Service
    class DifferentPasswordsError < BaseError
    end
  end
end
