require_relative '../../libraries/base_error'

module Users
  class Service
    class UsernameTooLongError < BaseError
    end
  end
end
