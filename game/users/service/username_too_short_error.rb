require_relative '../../libraries/base_error'

module Users
  class Service
    class UsernameTooShortError < BaseError
    end
  end
end
