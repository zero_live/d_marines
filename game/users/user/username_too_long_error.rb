require_relative '../../libraries/base_error'

module Users
  class User
    class UsernameTooLongError < BaseError
    end
  end
end
