require_relative '../../libraries/base_error'

module Users
  class User
    class DifferentPasswordsError < BaseError
    end
  end
end
