require_relative '../../libraries/base_error'

module Users
  class User
    class PasswordTooShortError < BaseError
    end
  end
end
