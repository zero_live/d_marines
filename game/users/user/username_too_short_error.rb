require_relative '../../libraries/base_error'

module Users
  class User
    class UsernameTooShortError < BaseError
    end
  end
end
