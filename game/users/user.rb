require_relative '../translations/service'
require_relative 'user/different_passowrds_error'
require_relative 'user/password_too_short_error'
require_relative 'user/username_too_short_error'
require_relative 'user/username_too_long_error'
require_relative '../libraries/enigma'

module Users
  class User
    USERNAME_MINIMUM_LENGHT = 5
    USERNAME_MAXIMUM_LENGHT = 10
    PASSWORD_MINIMUM_LENGHT = 8

    def self.from(item)
      username = item['username']
      password = Enigma.decrypt(item['password'])

      new(username, password, password)
    end

    def initialize(username, password, repeated_password)
      @username = username
      @password = Enigma.encrypt(password)

      raise DifferentPasswordsError.new(different_pass_message) if password != repeated_password
      raise PasswordTooShortError.new(pass_short_message) unless has_password_minimum_lenght?(password)
      raise UsernameTooShortError.new(name_short_message) unless has_username_minimum_lenght?
      raise UsernameTooLongError.new(name_long_message) unless is_username_too_long?
    end

    def ==(user)
      self.to_hash == user.to_hash
    end

    def to_hash
      {
        'username' => @username,
        'password' => @password
      }
    end

    private

    def has_username_minimum_lenght?
      @username.size >= USERNAME_MINIMUM_LENGHT
    end

    def has_password_minimum_lenght?(password)
      password.size >= PASSWORD_MINIMUM_LENGHT
    end

    def is_username_too_long?
      @username.size <= USERNAME_MAXIMUM_LENGHT
    end

    def different_pass_message
      Translations::Service.for('different_pass_message')
    end

    def pass_short_message
      Translations::Service.for('pass_short_message') % PASSWORD_MINIMUM_LENGHT
    end

    def name_short_message
      Translations::Service.for('name_short_message') % USERNAME_MINIMUM_LENGHT
    end

    def name_long_message
      Translations::Service.for('name_long_message') % USERNAME_MAXIMUM_LENGHT
    end
  end
end
