require_relative '../../libraries/base_error'

module Users
  class Collection
    class UsernameAlreadyTakenError < BaseError
    end
  end
end
