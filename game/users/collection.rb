require_relative '../translations/service'
require_relative 'collection/username_already_taken_error'
require_relative 'user'

module Users
  class Collection
    class << self
      @@collection = []

      def insert(user)
        raise UsernameAlreadyTakenError.new(already_taken_message) if exists?(user)

        @@collection.push(user.to_hash)

        user
      end

      def exists?(user)
        @@collection.any? do |item|
          collected_user = User.from(item)

          user == collected_user
        end
      end

      private

      def already_taken_message
        Translations::Service.for('already_taken_message')
      end
    end
  end
end
