require_relative 'collection/username_already_taken_error'
require_relative 'service/username_already_taken_error'
require_relative 'service/different_passwords_error'
require_relative 'service/username_too_short_error'
require_relative 'service/username_too_long_error'
require_relative 'service/password_too_short_error'
require_relative 'user/different_passowrds_error'
require_relative 'user/password_too_short_error'
require_relative 'user/username_too_short_error'
require_relative 'user/username_too_long_error'
require_relative 'collection'
require_relative 'user'

module Users
  class Service
    class << self
      DOES_NOT_EXIST = false
      EXISTS_ERRORS = [User::UsernameTooShortError, User::UsernameTooLongError, User::PasswordTooShortError]
      KNOWN_ERRORS = {
        Collection::UsernameAlreadyTakenError => UsernameAlreadyTakenError,
        User::UsernameTooShortError => UsernameTooShortError,
        User::DifferentPasswordsError => DifferentPasswordsError,
        User::UsernameTooLongError => UsernameTooLongError,
        User::PasswordTooShortError => PasswordTooShortError
      }

      def create(username, password, repeated_password)
        begin
          user = User.new(username, password, repeated_password)
          created_user = Collection.insert(user)

          created_user.to_hash
        rescue => error
          raise error_for(error)
        end
      end

      def exists?(username, password)
        begin
          user = User.new(username, password, password)
        rescue *EXISTS_ERRORS
          return DOES_NOT_EXIST
        end

        Collection.exists?(user)
      end

      private

      def error_for(error)
        listed_error = KNOWN_ERRORS[error.class]
        return listed_error.with(error) unless listed_error.nil?

        error
      end
    end
  end
end
